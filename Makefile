Makefile.PHONY: postgres restore dump destroy-postgres
.SHELL := $(shell which bash)
PROJECT_NAME=anastasiya_ga
PROJECT_USER_NAME=postgres

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


restore: ## Restore database
	sleep 20
	cat 'dump.sql' | docker exec -i $(PROJECT_NAME)-db psql -U $(PROJECT_USER_NAME) -d $(PROJECT_NAME)

dump: ## Create dump postgres database
		@docker exec -i $(PROJECT_NAME)-db  pg_dump -c -U $(PROJECT_USER_NAME) -d $(PROJECT_NAME) > dump.sql

postgres: ## Run postgres
	@if [ $(shell docker ps -qa -f name=$(PROJECT_NAME)-db) ]; then \
          docker start $(PROJECT_NAME)-db; \
     else \
		  docker run -d -p 5234:5432 \
				  -e POSTGRES_PASSWORD=$(PROJECT_NAME) \
				  -e POSTGRES_USER=$(PROJECT_USER_NAME) \
				  -e POSTGRES_DB=$(PROJECT_NAME) \
				  --name $(PROJECT_NAME)-db \
				  --restart always \
				  postgres:13; \
      fi

destroy-postgres: dump ## Destroy postgres
	@docker rm $(PROJECT_NAME)-db -f
