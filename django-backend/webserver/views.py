from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.conf import settings
from django.shortcuts import render


# Create your views here.
def test_req(request):
    return HttpResponse("<h1>I love you <3 </h1>")

def example_view(request):
    return render(request, 'test.html')


def redirector(request):
    return HttpResponseRedirect(settings.REDIRECT_TO)
